package hu.sonrisa.dojo.calculator;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link CalculatorService}
 *
 * @author aron
 */
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class CalculatorServiceTest {

  private CalculatorService calculatorService = new CalculatorServiceImpl();

  @Test
  public void testCalculateLoss01_Empty() {
    int loss = calculatorService.calculateLoss();
    assertEquals(0, loss);
  }

  @Test
  public void testCalculateLoss02_SimpleDesc() {
    int loss = calculatorService.calculateLoss(5, 0);
    assertEquals(-5, loss);
  }

  @Test
  public void testCalculateLoss03_SimpleAsc() {
    int loss = calculatorService.calculateLoss(5, 5);
    assertEquals(0, loss);
  }

  @Test
  public void testCalculateLoss04_FullDesc() {
    int loss = calculatorService.calculateLoss(5, 4, 3, 2, 1, 0);
    assertEquals(-5, loss);
  }

  @Test
  public void testCalculateLoss05_FullAsc() {
    int loss = calculatorService.calculateLoss(0, 1, 2, 3, 4, 5);
    assertEquals(0, loss);
  }

  @Test
  public void testCalculateLoss06_AscFlat() {
    int loss = calculatorService.calculateLoss(1, 2, 2, 4, 4, 4, 5, 5);
    assertEquals(0, loss);
  }

  @Test
  public void testCalculateLoss07_DescStart() {
    int loss = calculatorService.calculateLoss(5, 3, 4, 2, 3, 1);
    assertEquals(-4, loss);
  }

  @Test
  public void testCalculateLoss08_DescMiddle() {
    int loss = calculatorService.calculateLoss(3, 2, 4, 2, 1, 5);
    assertEquals(-3, loss);
  }

  @Test
  public void testCalculateLoss09_DescRandom() {
    int[] params = new int[100];
    params[0] = 100;
    for (int loop = 0; loop < 100; loop++) {
      for (int i = 99; i > 0; i--) {
        params[100 - i] = ThreadLocalRandom.current().nextInt(0, i);
      }
      int loss = calculatorService.calculateLoss(params);
      assertEquals(-100, loss);
    }
  }

  @Test
  public void testCalculateStockContributions10_Impossible() {
    int[] contributions = calculatorService.calculateStockContribution(100, 10, 20, 30);
    assertEquals(3, contributions.length);
    for (int i = 0 ; i < contributions.length; i++) {
      assertEquals(0, contributions[i]);
    }
  }

  @Test
  public void testCalculateStockContributions11_Sort() {
    int[] contributions = calculatorService.calculateStockContribution(100, 100, 3, 1, 2);
    assertEquals(4, contributions.length);
    assertEquals(1, contributions[0]);
    assertEquals(2, contributions[1]);
    assertEquals(3, contributions[2]);
    assertEquals(94, contributions[3]);
  }

  @Test
  public void testCalculateStockContributions12_BudgetUnlimited() {
    int[] contributions = calculatorService.calculateStockContribution(30, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
    assertEquals(3, contributions.length);
    assertEquals(10, contributions[0]);
    assertEquals(10, contributions[1]);
    assertEquals(10, contributions[2]);
  }

  @Test
  public void testCalculateStockContributions13_SeveralSolutions() {
    int[] contributions = calculatorService.calculateStockContribution(101, 40, 40, 40);
    assertEquals(3, contributions.length);
    assertEquals(33, contributions[0]);
    assertEquals(34, contributions[1]);
    assertEquals(34, contributions[2]);
  }

  @Test
  public void testCalculateStockContributions14_TooManyContributors() {
    int[] contributions = calculatorService.calculateStockContribution(5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    assertEquals(10, contributions.length);
    for (int i = 0 ; i < 5; i++) {
      assertEquals(0, contributions[i]);
    }
    for (int i = 5 ; i < 10; i++) {
      assertEquals(1, contributions[i]);
    }
  }

  @Test
  public void testCalculateStockContributions15_NoContributors() {
    int[] contributions = calculatorService.calculateStockContribution(100);
    assertEquals(0, contributions.length);
  }

  @Test
  public void testCalculateStockContributions16_FreeStock() {
    int[] contributions = calculatorService.calculateStockContribution(0, 10, 10, 10);
    assertEquals(3, contributions.length);
    assertEquals(0, contributions[0]);
    assertEquals(0, contributions[1]);
    assertEquals(0, contributions[2]);
  }

  @Test
  public void testCalculateStockContributions17_HugeNoOfContributors() {
    int max = 1_000_000;
    int[] budgets = new int[max];
    Arrays.fill(budgets, 1);
    int[] contributions = calculatorService.calculateStockContribution(max, budgets);
    assertEquals(max, contributions.length);
    for (int i = 0 ; i < max; i++) {
      assertEquals(1, contributions[i]);
    }
  }

}
