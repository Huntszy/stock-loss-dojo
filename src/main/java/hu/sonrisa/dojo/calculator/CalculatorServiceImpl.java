package hu.sonrisa.dojo.calculator;

/**
 * Calculation service implementation
 *
 * @author aron
 */
public class CalculatorServiceImpl implements CalculatorService {

  /**
   * {@inheritDoc}
   */
  @Override
  public int calculateLoss(final int... stockValues) {
    //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY
    return Integer.MAX_VALUE;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int[] calculateStockContribution(final int fullPrice, final int... budgets) {
    //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE SECOND STORY
    return null;
  }

}
