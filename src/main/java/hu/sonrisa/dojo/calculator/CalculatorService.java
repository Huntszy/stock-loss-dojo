package hu.sonrisa.dojo.calculator;

/**
 * Calculation service interface
 *
 * Methods based on https://www.codingame.com
 *
 * @author aron
 */
public interface CalculatorService {

  /**
   * The method analyze a chronological series of stock values in order to show the largest loss
   * that it is possible to make by buying a share at a given time t0 and by selling it at a later date t1.
   * The loss will be expressed as the difference in value between t0 and t1.
   * If there is no loss, the loss will be worth 0.
   *
   * @param stockValues the stock values arranged in order,
   *                    from the date of their introduction on the stock market v1
   *                    until the last known value vn. The values are integers.
   * @return The maximal loss p, expressed negatively if there is a loss, otherwise 0.
   */
  int calculateLoss(int... stockValues);

  /**
   * The method checks if the contributors have enough money to buy the stock, and if so,
   * calculates how much each contributor should pay, according to their respective budget limit.
   *
   * No one can pay more than his maximum budget.
   * The optimal solution is the one for which the highest contribution is minimal.
   * If there are several optimal solutions,
   * then the best solution is the one for which the highest second contribution is minimal,
   * and so on and so forth...
   * If the full price greater than the sum of budgets, return 0 contribution for each contributor.
   *
   * @param fullPrice the full price of the stock
   * @param budgets the budgets of the contributors
   * @return contributions, sorted in ascending order.
   */
  int[] calculateStockContribution(int fullPrice, int... budgets);

}
