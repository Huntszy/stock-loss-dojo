package hu.sonrisa;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class StockCalculationDojoApplication {

  public static void main(final String[] args) {
    SpringApplication.run(StockCalculationDojoApplication.class, args); //NOSONAR
  }
}
