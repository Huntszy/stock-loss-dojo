# Stock calculation
Demo solution can be found on the “demo_solution” branch.

### Source: https://www.codingame.com

### Technology
  - Maven 3
  - Java 8

## Introduction
Both of the stories can be a separate dojo sesion because of the length of the stories.
When you create your solution you should run the tests often!

To resolve the first story you will need to create and implement 
a calculateLoss() method on the CalculatorService class.

To resolve the second sotry you will need to create and implement 
a calculateStockContribution() method on the CalculatorService class.

## Stories 

### 1. Calculate loss of the stock
You have to analyze a chronological series of stock values. Imagine a line diagram where the axis y means 
the stock value and the axis x means the time. In this diagram you have to find the largest loss in the values. 
The loss will be expressed as the difference in value between t0-t1, t1-t2, etc. 
If there is no loss, the loss will be worth 0.
Be carefull, loss not only means constant decrease in chronological values.
 
- Input: Array of chronological stock values. The values are integers. 
- Output: The maximal loss p, expressed negatively if there is a loss, otherwise 0.


### 2. Calculate the contributions needed to buy a stock
The method checks if the contributors have enough money to buy the stock, and if so, calculates how much 
each contributor should pay, according to their respective budget limit.
Rules:
1. No one can pay more than his maximum budget.
2. The optimal solution is the one for which the highest contribution is minimal.
3. If there are several optimal solutions, then the best solution is the one for which the highest second contribution 
is minimal, and so on and so forth...
4. If the full price greater than the sum of budgets, return 0 contribution for each contributor.

- Input: The full price of the stock. The budgets of the contributors. 
- Output: Contributions, sorted in ascending order.


## Extras
Two tools are integrated in pom to play with.
Feel free to try them at production code! :)

#### Dependency check
http://jeremylong.github.io/DependencyCheck/index.html

With a maven build you could generate a dependency report with possible vulnerable dependencies.

#### Pitest mutation testing
http://pitest.org/

With a maven build you could generate a mutation testing report to check if your tests fails 
if the code is changed (mutated) or not.